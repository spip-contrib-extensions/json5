# JSON5 dans SPIP

[Json5](https://json5.org/) est une extension de JSON un peu plus tolérante, particulièrement :

- sur les virgules finales `[3, 4,]`
- les identifiants (non numériques) sans apostrophe `{id: 34, name: 'toto'}`

Ce plugin fournit juste la librairie [colinodell/json5](https://github.com/colinodell/json5) qui est une implémentation PHP du décodage de [json5](https://json5.org/).

Pour charger la fonction `json5_decode` dans SPIP, utiliser `include_spip('inc/json5')`;
C’est la même signature que [`json_decode`](https://www.php.net/manual/fr/function.json-decode.php).

```php
include_spip('inc/json5');
$data = json5_decode("{id: 34, name: 'toto', }", true);
// $data['id'] = 34
```