<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/mots_techniques.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// O
	'json5_description' => 'Intègre https://github.com/colinodell/json5 dans SPIP',
	'json5_slogan' => 'Intégration de Json5',
	'json5_titre' => 'Json5'
);
